FROM ubuntu:20.04
USER root
RUN apt update && apt upgrade -y && apt dist-upgrade -y
RUN  apt-get update \
  && apt-get install -y wget \
  && rm -rf /var/lib/apt/lists/*
RUN wget https://github.com/aquasecurity/trivy/releases/download/v0.18.0/trivy_0.18.0_Linux-64bit.deb
RUN dpkg -i trivy_0.18.0_Linux-64bit.deb